# Advent Of Code

My implementation of [Advent of Code](http://adventofcode.com) in Python.

## Use cases

Run tests

    cd adventofcode && python runtests.py

Create new day

    python adventofcode/newday.py 987
