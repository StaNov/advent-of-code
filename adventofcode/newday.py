import fileinput
import os
import pathlib
import re
import shutil
from argparse import *

parser = ArgumentParser()
parser.add_argument("day_number", type=int, help="Number of day to create")

args = parser.parse_args()

folder_name = f"day{args.day_number:02}"
print(f"Creating new day '{folder_name}'")
this_dir = pathlib.Path(__file__).parent.absolute()

shutil.copytree(f"{this_dir}/problems/days/template", f"{this_dir}/problems/days/{folder_name}")

for path in pathlib.Path(f"{this_dir}/problems/days/{folder_name}").rglob("*.py"):
    with fileinput.FileInput(path, inplace=True) as file:
        replace_template_at_next_line = False
        comment_next_line_and_add_todo = False
        for line in file:
            if "%REPLACE_TEMPLATE_WITH_DAYXXX%" in line:
                replace_template_at_next_line = True
            elif "%COMMENT_NEXT_LINE_AND_ADD_TODO%" in line:
                comment_next_line_and_add_todo = True
            elif replace_template_at_next_line:
                print(line.replace("template", folder_name), end='')
                replace_template_at_next_line = False
            elif comment_next_line_and_add_todo:
                print(re.sub(r"^(\s*)(.*)$", r"\1# TODO\n\1# \2\n\1pass", line), end='')
                comment_next_line_and_add_todo = False
            else:
                print(line, end='')

print(f"Day directory '{folder_name}' created successfully. Happy coding!")
