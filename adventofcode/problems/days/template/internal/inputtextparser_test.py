from .inputtextparser import InputTextParser


def test_parse():
    # %COMMENT_NEXT_LINE_AND_ADD_TODO%
    assert InputTextParser().parse("abc\ndef\nghi") == ["abc", "def", "ghi"]
