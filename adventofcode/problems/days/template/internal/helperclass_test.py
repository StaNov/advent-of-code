import time

import pytest

from adventofcode.problems.framework import testsuite
from . import HelperClass


@pytest.fixture
def hex_grid():
    return HelperClass()


def test_helper_method(hex_grid):
    # %COMMENT_NEXT_LINE_AND_ADD_TODO%
    assert hex_grid.helper_method("test") == 0


@testsuite.time_expensive
def test_time_expensive_test():
    # %COMMENT_NEXT_LINE_AND_ADD_TODO%
    time.sleep(1)
