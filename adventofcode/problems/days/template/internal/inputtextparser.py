from adventofcode.problems.framework import DefaultInputTextParser


class InputTextParser(DefaultInputTextParser):
    def parse(self, input_string):
        # %COMMENT_NEXT_LINE_AND_ADD_TODO%
        return input_string.splitlines()
