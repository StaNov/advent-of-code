import pytest

from .solver import Solver


@pytest.fixture
def solver():
    return Solver()


def test_1_something(solver):
    # %COMMENT_NEXT_LINE_AND_ADD_TODO%
    assert solver.solve_1("test test") == 0


def test_2_something(solver):
    # %COMMENT_NEXT_LINE_AND_ADD_TODO%
    assert solver.solve_2("test test") == 0
