import re

from adventofcode.problems.framework import DefaultInputTextParser


class InputTextParser(DefaultInputTextParser):
    def parse(self, input_string):
        lines = input_string.splitlines()

        result = []
        for line in lines:
            low, high, char, password = re.compile(r"(\d+)-(\d+) (\w): (\w+)").search(line).groups()
            result.append((int(low), int(high), char, password))

        return result
