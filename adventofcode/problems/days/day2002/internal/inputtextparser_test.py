from .inputtextparser import InputTextParser


def test_parse():
    assert InputTextParser().parse("123-456 c: pass\n5-10 d: word") == [
        (123, 456, "c", "pass"),
        (5, 10, "d", "word"),
    ]
