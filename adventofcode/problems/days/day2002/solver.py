from adventofcode.problems.framework import AbstractSolver


class Solver(AbstractSolver):
    def _solve_1_internal(self, input_):
        result = 0
        for low, high, char, password in input_:
            count = password.count(char)
            if low <= count <= high:
                result += 1

        return result

    def _solve_2_internal(self, input_):
        result = 0
        for first, second, char, password in input_:
            if (password[first - 1] == char) != (password[second - 1] == char) :
                result += 1

        return result
