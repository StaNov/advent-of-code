import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


def test_example_1_1(calculator):
    assert DayCalculator("""
1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
""").calculate_part_1() == 2


def test_main_1(calculator):
    assert calculator.calculate_part_1() == 398


def test_example_2_1(calculator):
    assert DayCalculator("""
1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
""").calculate_part_2() == 1


def test_main_2(calculator):
    assert calculator.calculate_part_2() == 562
