import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


def test_example_1_1(calculator):
    assert DayCalculator("""
1721
979
366
299
675
1456
""").calculate_part_1() == 514579


def test_main_1(calculator):
    assert calculator.calculate_part_1() == 713184


def test_example_2_1(calculator):
    assert DayCalculator("""
1721
979
366
299
675
1456
""").calculate_part_2() == 241861950


def test_main_2(calculator):
    assert calculator.calculate_part_2() == 261244452
