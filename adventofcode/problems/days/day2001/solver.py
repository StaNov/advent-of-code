from adventofcode.problems.framework import AbstractSolver


class Solver(AbstractSolver):
    def _solve_1_internal(self, input_):
        for i, x in enumerate(input_):
            for y in input_[i:]:
                if x + y == 2020:
                    return x * y

    def _solve_2_internal(self, input_):
        for i, x in enumerate(input_):
            for j, y in enumerate(input_[i:]):
                for z in input_[j:]:
                    if x + y + z == 2020:
                        return x * y * z
