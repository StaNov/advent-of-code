from .inputtextparser import InputTextParser


def test_parse():
    assert InputTextParser().parse("123\n456\n") == [123, 456]
