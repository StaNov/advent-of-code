from adventofcode.problems.framework import AbstractSolver
from .internal import Pincher, Densifier, Hexifier


class Solver(AbstractSolver):
    def __init__(self):
        super().__init__()
        self.pincher = Pincher()
        self.densifier = Densifier()
        self.hexifier = Hexifier()

    def _solve_1_internal(self, input_):
        lengths = input_[0]
        self.pincher.pinch_lengths(lengths)
        current_list = self.pincher.current_list
        return current_list[0] * current_list[1]

    def _solve_2_internal(self, input_):
        lengths = list(map(ord, input_[1]))
        lengths += [17, 31, 73, 47, 23]
        for _ in range(64):
            self.pincher.pinch_lengths(lengths)
        sparse_hash = self.pincher.current_list
        dense_hash = self.densifier.densify(sparse_hash)
        return self.hexifier.hexify(dense_hash)
