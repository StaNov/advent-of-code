import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


def test_no_pinching(calculator):
    assert DayCalculator("").calculate_part_1() == 0 * 1


def test_pinching_4_gets_no_zero_to_beginning(calculator):
    assert DayCalculator("4").calculate_part_1() == 3 * 2


def test_main_1(calculator):
    assert calculator.calculate_part_1() == 13760


@pytest.mark.parametrize("input_string,expected_hash", [
    ("", "a2582a3a0e66e6e86e3812dcb672a272"),
    ("AoC 2017", "33efeb34ea91902bb2f59c9920caa6cd"),
    ("1,2,3", "3efbe78a8d82f29979031a4aa0b16a9d"),
    ("1,2,4", "63960835bcdc130f0b66d7ff4f6a5a8e"),
])
def test_part_2(input_string, expected_hash):
    assert DayCalculator(input_string).calculate_part_2() == expected_hash


def test_main_2(calculator):
    assert calculator.calculate_part_2() == "2da93395f1a6bb3472203252e3b17fe5"
