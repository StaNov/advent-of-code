from .inputtextparser import InputTextParser


def test_parse_empty():
    assert InputTextParser().parse("") == ([], "")


def test_parse_value_error():
    assert InputTextParser().parse("aaa") == (None, "aaa")


def test_parse():
    assert InputTextParser().parse("1,2,3") == ([1, 2, 3], "1,2,3")
