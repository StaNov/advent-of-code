from adventofcode.problems.days.day210.internal.densifier import Densifier


def test_empty_input():
    assert Densifier().densify([]) == []


def test_16_values():
    assert Densifier().densify([65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22]) == [64]


def test_32_values():
    assert Densifier().densify([
        65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22,
        65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22
    ]) == [64, 64]
