class Hexifier:
    def hexify(self, numbers):
        result = ""
        for i in numbers:
            result += str(hex(i))[2:].zfill(2)
        return result
