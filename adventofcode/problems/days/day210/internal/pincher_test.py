import pytest

from . import Pincher


@pytest.mark.parametrize("lengths,expected_prefix", [
    [[], list(range(256))],
    [[4], [3, 2, 1, 0, 4]],
    [[2, 3], [1, 0, 4, 3, 2, 5]],
    [[2, 2, 2, 2], [1, 0, 3, 2, 4, 6, 5, 7, 8, 10, 9, 11]],
])
def test_pinch_with_default_length(lengths, expected_prefix):
    test_pinch(lengths, expected_prefix, None)


@pytest.mark.parametrize("lengths,expected_prefix,list_length_override", [
    [[1, 4], [1, 0, 3, 2], 4],
])
def test_pinch(lengths, expected_prefix, list_length_override):
    pincher = Pincher() if list_length_override is None else Pincher(list_length_override)
    pincher.pinch_lengths(lengths)
    assert pincher.current_list[:len(expected_prefix)] == expected_prefix


def test_full_example():
    pincher = Pincher(5)
    pincher.pinch_lengths([3, 4, 1, 5])
    assert pincher.current_list == [3, 4, 2, 1, 0]
