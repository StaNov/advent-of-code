class Pincher:
    def __init__(self, list_length=256):
        self.current_list = list(range(list_length))
        self._position = 0
        self._skip_length = 0

    def pinch_lengths(self, lengths):
        for length in lengths:
            self.pinch_length(length)

    def pinch_length(self, length):
        to_reverse = []
        for i in range(length):
            to_reverse.append(self.current_list[self._normalized_index(self._position + i)])

        reversed_ = reversed(to_reverse)

        for i, number in enumerate(reversed_):
            self.current_list[self._normalized_index(self._position + i)] = number

        self._position = self._normalized_index(self._position + length + self._skip_length)
        self._skip_length += 1

    def _normalized_index(self, index):
        return index % len(self.current_list)
