class Densifier:
    def densify(self, sparse_hash):
        result = []
        remaining = list(sparse_hash)

        while len(remaining) >= 16:
            chunk_result = self._process_chunk(remaining[:16])
            result.append(chunk_result)
            remaining = remaining[16:]

        return result

    @staticmethod
    def _process_chunk(chunk):
        result = chunk[0]
        for i in chunk[1:]:
            result ^= i
        return result
