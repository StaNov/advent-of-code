from adventofcode.problems.framework import DefaultInputTextParser


class InputTextParser(DefaultInputTextParser):
    def parse(self, input_string):
        if not input_string:
            return [], ""

        try:
            parsed = [int(s) for s in input_string.split(",")]
        except ValueError:
            parsed = None

        return parsed, input_string
