from adventofcode.problems.days.day210.internal.hexifier import Hexifier


def test_hexify_empty():
    assert Hexifier().hexify([]) == ""


def test_hexify_one_number():
    assert Hexifier().hexify([123]) == "7b"


def test_hexify_zero_beginning():
    assert Hexifier().hexify([1]) == "01"


def test_hexify_multiple_numbers():
    assert Hexifier().hexify([123, 124, 125]) == "7b7c7d"
