import pytest

from . import RegisterCalculator


@pytest.fixture
def calculator():
    return RegisterCalculator()


def test_zero_is_highest_value_after_calculator_is_created(calculator):
    assert 0 == calculator.highest_value_current


def test_apply_one_instruction(calculator):
    calculator.apply_instruction(
        InstructionMock("a", 10)
    )
    assert 10 == calculator.highest_value_current


def test_apply_two_instructions(calculator):
    calculator.apply_instruction(
        InstructionMock("a", 20)
    )
    calculator.apply_instruction(
        InstructionMock("b", 10)
    )
    assert 20 == calculator.highest_value_current


def test_apply_two_instructions_same_register(calculator):
    calculator.apply_instruction(
        InstructionMock("a", 20)
    )
    calculator.apply_instruction(
        InstructionMock("a", 10)
    )
    assert 30 == calculator.highest_value_current


def test_highest_value_all_time_is_zero_after_creation(calculator):
    assert calculator.highest_value_all_time == 0


def test_highest_value_all_time_after_one_instruction(calculator):
    calculator.apply_instruction(
        InstructionMock("a", 20)
    )

    assert calculator.highest_value_all_time == 20


def test_highest_value_all_time_after_value_being_dropped(calculator):
    calculator.apply_instruction(
        InstructionMock("a", 20)
    )

    calculator._registers.add("a", -10)

    assert calculator.highest_value_current == 10
    assert calculator.highest_value_all_time == 20


class InstructionMock:

    def __init__(self, register_name, value_to_add):
        self._register_name = register_name
        self._value_to_add = value_to_add

    def apply_on_registers(self, registers):
        registers.add(self._register_name, self._value_to_add)
