from .registers import Registers


class RegisterCalculator:
    def __init__(self):
        self._registers = Registers()
        self._highest_all_time = 0

    def apply_instruction(self, instruction):
        instruction.apply_on_registers(self._registers)
        self._highest_all_time = max(self._highest_all_time, self.highest_value_current)

    @property
    def highest_value_current(self):
        values = [value for _, value in self._registers.registers]
        return max(values, default=0)

    @property
    def highest_value_all_time(self):
        return self._highest_all_time
