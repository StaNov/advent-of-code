from adventofcode.problems.days.day208.internal import InstructionConditionType


def test_greater():
    assert InstructionConditionType.GREATER.is_true(2, 1)
    assert not InstructionConditionType.GREATER.is_true(1, 1)


def test_greater_or_equal():
    assert InstructionConditionType.GREATER_EQUALS.is_true(2, 1)
    assert InstructionConditionType.GREATER_EQUALS.is_true(1, 1)
    assert not InstructionConditionType.GREATER_EQUALS.is_true(1, 2)


def test_lesser():
    assert InstructionConditionType.LESSER.is_true(1, 2)
    assert not InstructionConditionType.LESSER.is_true(1, 1)


def test_lesser_or_equal():
    assert InstructionConditionType.LESSER_EQUALS.is_true(1, 2)
    assert InstructionConditionType.LESSER_EQUALS.is_true(1, 1)
    assert not InstructionConditionType.LESSER_EQUALS.is_true(2, 1)


def test_equals():
    assert InstructionConditionType.EQUALS.is_true(1, 1)
    assert not InstructionConditionType.EQUALS.is_true(1, 2)


def test_not_equals():
    assert InstructionConditionType.NOT_EQUALS.is_true(1, 2)
    assert not InstructionConditionType.NOT_EQUALS.is_true(1, 1)
