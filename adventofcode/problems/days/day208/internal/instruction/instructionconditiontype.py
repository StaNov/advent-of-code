from enum import Enum


class InstructionConditionType(Enum):
    GREATER = 0
    GREATER_EQUALS = 1
    LESSER = 2
    LESSER_EQUALS = 3
    EQUALS = 4
    NOT_EQUALS = 5

    def is_true(self, a, b):
        if self is InstructionConditionType.GREATER:
            return a > b
        if self is InstructionConditionType.GREATER_EQUALS:
            return a >= b
        if self is InstructionConditionType.LESSER:
            return a < b
        if self is InstructionConditionType.LESSER_EQUALS:
            return a <= b
        if self is InstructionConditionType.EQUALS:
            return a == b
        if self is InstructionConditionType.NOT_EQUALS:
            return a != b
        else:
            raise Exception("Unknown type")
