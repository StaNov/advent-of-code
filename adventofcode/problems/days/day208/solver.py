from adventofcode.problems.framework import AbstractSolver
from .internal import RegisterCalculator


class Solver(AbstractSolver):
    def __init__(self):
        super().__init__()
        self.calculator = RegisterCalculator()

    def _solve_1_internal(self, input_):
        self._apply_instructions(input_)
        return self.calculator.highest_value_current

    def _solve_2_internal(self, input_):
        self._apply_instructions(input_)
        return self.calculator.highest_value_all_time

    def _apply_instructions(self, input_):
        for instruction in input_.instructions:
            self.calculator.apply_instruction(instruction)
