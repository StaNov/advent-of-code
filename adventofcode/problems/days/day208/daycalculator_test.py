import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def day_calculator():
    return DayCalculator()


def test_main_1_on_empty_input():
    assert 0 == DayCalculator("").calculate_part_1()


def test_main_1_with_all_conditions_true():
    custom_input = (
        "a inc 10 if a == 0\n"
        "a inc 10 if b <= 0\n"
        "a inc 10 if c > -1\n"
        "a inc 10 if d < 2\n"
        "a dec -10 if e >= 0")

    assert 50 == DayCalculator(custom_input).calculate_part_1()


def test_main_1_with_all_conditions_false():
    custom_input = (
        "a inc 10 if a == 1\n"
        "a inc 10 if b <= -10\n"
        "a inc 10 if c > 0\n"
        "a inc 10 if d < 0\n"
        "a dec -10 if e >= 1")

    assert DayCalculator(custom_input).calculate_part_1() == 0


def test_main_1_full_example():
    custom_input = (
        "b inc 5 if a > 1\n"
        "a inc 1 if b < 5\n"
        "c dec -10 if a >= 1\n"
        "c inc -20 if c == 10\n")

    assert DayCalculator(custom_input).calculate_part_1() == 1


def test_main_1(day_calculator):
    assert day_calculator.calculate_part_1() == 5075


def test_main_2_full_example():
    custom_input = (
        "b inc 5 if a > 1\n"
        "a inc 1 if b < 5\n"
        "c dec -10 if a >= 1\n"
        "c inc -20 if c == 10\n")

    assert DayCalculator(custom_input).calculate_part_2() == 10


def test_main_2(day_calculator):
    assert day_calculator.calculate_part_2() == 7310
