import re


class GarbageHandler(object):
    @staticmethod
    def remove_garbage(param):
        return re.sub("<.*?>", "", param)

    def garbage_size(self, param):
        garbage_sequences_count = len(list(filter(lambda char: char == ">", param)))
        original_length = len(param)
        length_without_garbage = len(self.remove_garbage(param))

        return original_length - length_without_garbage - 2 * garbage_sequences_count
