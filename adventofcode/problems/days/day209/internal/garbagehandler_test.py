import pytest

from adventofcode.problems.days.day209.internal.garbagehandler import GarbageHandler


def test_remove_garbage():
    assert GarbageHandler().remove_garbage("a<b<c>d") == "ad"


@pytest.mark.parametrize("argument,expected", [
    ("<>", 0),
    ("<random characters>", 17),
    ("<<<<>", 3),
    ("<{}>", 2),
    ("<{o\"i,<{i<a>", 10),
])
def test_garbage_size(argument, expected):
    assert GarbageHandler().garbage_size(argument) == expected
