class GroupCounter:
    def __init__(self):
        pass

    @staticmethod
    def count_groups(param):
        result = 0
        current_indent = 0
        for char in param:
            if char == "{":
                current_indent += 1
            elif char == "}":
                result += current_indent
                current_indent -= 1
        return result
