from .inputtextparser import InputTextParser


def test_regular_text():
    assert InputTextParser().parse("abc") == "abc"


def test_remove_ignored_characters():
    assert InputTextParser().parse("a!bc!de!!f") == "acef"
