import pytest

from . import GroupCounter


@pytest.fixture
def group_counter():
    return GroupCounter()


def test_no_groups(group_counter):
    assert group_counter.count_groups("") == 0


def test_one_group(group_counter):
    assert group_counter.count_groups("{}") == 1


def test_nested_group(group_counter):
    assert group_counter.count_groups("{{}}") == 3


def test_same_level_groups(group_counter):
    assert group_counter.count_groups("{{},{},{}}") == 7
