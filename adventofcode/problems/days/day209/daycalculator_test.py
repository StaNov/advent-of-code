import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


def test_empty_input(calculator):
    assert DayCalculator("").calculate_part_1() == 0


def test_one_group(calculator):
    assert DayCalculator("{}").calculate_part_1() == 1


def test_multiple_groups(calculator):
    assert DayCalculator("{{{}}}").calculate_part_1() == 6
    assert DayCalculator("{{},{}}").calculate_part_1() == 5
    assert DayCalculator("{{{},{},{{}}}}").calculate_part_1() == 16


def test_groups_with_garbage(calculator):
    assert DayCalculator("{{<ab>},{<ab>},{<ab>},{<ab>}}").calculate_part_1() == 9
    assert DayCalculator("{{<!!>},{<!!>},{<!!>},{<!!>}}").calculate_part_1() == 9
    assert DayCalculator("{{<a!>},{<a!>},{<a!>},{<ab>}}").calculate_part_1() == 3


def test_main_1(calculator):
    assert calculator.calculate_part_1() == 16689


@pytest.mark.parametrize("argument,expected", [
    ("<>", 0),
    ("<random characters>", 17),
    ("<<<<>", 3),
    ("<{!>}>", 2),
    ("<!!>", 0),
    ("<!!!>>", 0),
    ("<{o\"i!a,<{i<a>", 10),
])
def test_example_2(calculator, argument, expected):
    assert DayCalculator(argument).calculate_part_2() == expected


def test_main_2(calculator):
    assert calculator.calculate_part_2() == 7982
