from adventofcode.problems.framework import AbstractSolver
from .internal import GroupCounter, GarbageHandler


class Solver(AbstractSolver):
    def __init__(self):
        super().__init__()
        self.group_counter = GroupCounter()
        self.garbage_handler = GarbageHandler()

    def _solve_1_internal(self, input_):
        input_ = self.garbage_handler.remove_garbage(input_)
        return self.group_counter.count_groups(input_)

    def _solve_2_internal(self, input_):
        return self.garbage_handler.garbage_size(input_)
