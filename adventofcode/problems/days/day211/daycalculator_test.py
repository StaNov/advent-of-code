import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


@pytest.mark.parametrize("input_string,expected", [
    ("", 0),
    ("n,n,n", 3),
    ("n,n,s", 1),
    ("ne,ne,ne", 3),
    ("ne,n", 2),
    ("ne,ne,sw,sw", 0),
    ("ne,ne,s,s", 2),
    ("se,sw,se,sw,sw", 3),
    ("n,se,s,sw,nw,n,ne,s", 0),
])
def test_full_example_1(input_string, expected):
    assert DayCalculator(input_string).calculate_part_1() == expected


def test_main_1(calculator):
    assert calculator.calculate_part_1() == 808


def test_main_2(calculator):
    assert calculator.calculate_part_2() == 1556
