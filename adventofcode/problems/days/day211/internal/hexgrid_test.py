import pytest

from . import HexGrid
from .direction import Direction


@pytest.fixture
def hex_grid():
    return HexGrid()


def test_init(hex_grid):
    assert hex_grid.distance == 0
    assert hex_grid.max_distance == 0
    assert hex_grid.position == (0, 0)


def test_move(hex_grid):
    hex_grid.move(Direction.NORTH)
    hex_grid.move(Direction.NORTH)
    hex_grid.move(Direction.NORTH)
    assert hex_grid.position == (0, 3)


def test_move_directions(hex_grid):
    hex_grid.move_directions([Direction.SOUTH, Direction.SOUTH, Direction.SOUTH])
    assert hex_grid.position == (0, -3)


@pytest.mark.parametrize("x,y,expected", [
    (0, 0, 0),
    (0, 1, 1),
    (0, 2, 2),

    (1, 0, 1),
    (1, 1, 2),
    (1, 2, 3),

    (2, 1, 2),
    (2, 2, 3),
    (2, 3, 4),

    (3, 1, 3),
    (3, 2, 4),
    (3, 3, 5),

    (4, 2, 4),
    (4, 3, 5),
    (4, 4, 6),

    (5, 2, 5),
    (5, 3, 6),
    (5, 4, 7),

    (6, 3, 6),
    (6, 4, 7),
    (6, 5, 8),

    (7, 3, 7),
    (7, 4, 8),
    (7, 5, 9),


    (0, 0, 0),
    (0, -1, 1),
    (0, -2, 2),

    (1, -1, 1),
    (1, -2, 2),
    (1, -3, 3),

    (2, -1, 2),
    (2, -2, 3),
    (2, -3, 4),

    (3, -2, 3),
    (3, -3, 4),
    (3, -4, 5),

    (4, -2, 4),
    (4, -3, 5),
    (4, -4, 6),

    (5, -3, 5),
    (5, -4, 6),
    (5, -5, 7),
])
def test_distance(hex_grid, x, y, expected):
    hex_grid.position = (x, y)
    assert hex_grid.distance == expected


def test_max_distance(hex_grid):
    hex_grid.move_directions([Direction.SOUTH, Direction.SOUTH, Direction.NORTH, Direction.NORTH])
    assert hex_grid.max_distance == 2
