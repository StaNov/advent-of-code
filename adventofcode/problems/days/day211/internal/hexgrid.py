class HexGrid:
    def __init__(self):
        self.position = (0, 0)
        self.max_distance = 0

    @property
    def distance(self):
        x = abs(self.position[0])

        if self.position[1] >= 0:
            y = abs(self.position[1])
        else:
            y = abs(self.position[1]) - x % 2

        return x + max(0, y - x//2)

    def move_directions(self, directions):
        for direction in directions:
            self.move(direction)

    def move(self, direction):
        self.position = direction.apply(self.position)
        self.max_distance = max(self.max_distance, self.distance)
