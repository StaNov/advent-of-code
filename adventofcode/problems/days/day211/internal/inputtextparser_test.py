from .direction import Direction
from .inputtextparser import InputTextParser


def test_parse_empty():
    assert InputTextParser().parse("") == []


def test_parse():
    assert InputTextParser().parse("n,s,ne,sw,nw,se") == [
        Direction.NORTH,
        Direction.SOUTH,
        Direction.NORTH_EAST,
        Direction.SOUTH_WEST,
        Direction.NORTH_WEST,
        Direction.SOUTH_EAST,
    ]
