from enum import Enum


class Direction(Enum):
    NORTH = 0
    SOUTH = 1
    NORTH_EAST = 2
    SOUTH_WEST = 3
    NORTH_WEST = 4
    SOUTH_EAST = 5

    def apply(self, position):
        if self == Direction.NORTH:
            return position[0], position[1] + 1
        elif self == Direction.SOUTH:
            return position[0], position[1] - 1
        elif self == Direction.NORTH_EAST:
            return position[0] + 1, position[1] + (1 if position[0] % 2 == 1 else 0)
        elif self == Direction.SOUTH_WEST:
            return position[0] - 1, position[1] - (1 if position[0] % 2 == 0 else 0)
        elif self == Direction.NORTH_WEST:
            return position[0] - 1, position[1] + (1 if position[0] % 2 == 1 else 0)
        elif self == Direction.SOUTH_EAST:
            return position[0] + 1, position[1] - (1 if position[0] % 2 == 0 else 0)
