from adventofcode.problems.days.day211.internal.direction import Direction
from adventofcode.problems.framework import DefaultInputTextParser

_directions = {
    "n": Direction.NORTH,
    "s": Direction.SOUTH,
    "ne": Direction.NORTH_EAST,
    "sw": Direction.SOUTH_WEST,
    "nw": Direction.NORTH_WEST,
    "se": Direction.SOUTH_EAST,
}


class InputTextParser(DefaultInputTextParser):
    def parse(self, input_string):
        if not input_string:
            return []

        return list(map(self._parse_direction, input_string.split(",")))

    @staticmethod
    def _parse_direction(direction):
        return _directions[direction]
