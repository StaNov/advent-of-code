from adventofcode.problems.days.day211.internal.direction import Direction


def test_apply_north():
    assert Direction.NORTH.apply((5, 10)) == (5, 11)


def test_apply_south():
    assert Direction.SOUTH.apply((5, 10)) == (5, 9)


def test_apply_north_east_even_x():
    assert Direction.NORTH_EAST.apply((0, 0)) == (1, 0)


def test_apply_north_east_odd_x():
    assert Direction.NORTH_EAST.apply((1, 0)) == (2, 1)


def test_apply_south_west_even_x():
    assert Direction.SOUTH_WEST.apply((0, 0)) == (-1, -1)


def test_apply_south_west_odd_x():
    assert Direction.SOUTH_WEST.apply((-1, -1)) == (-2, -1)


def test_apply_north_west_even_x():
    assert Direction.NORTH_WEST.apply((0, 0)) == (-1, 0)


def test_apply_north_west_odd_x():
    assert Direction.NORTH_WEST.apply((-1, 0)) == (-2, 1)


def test_apply_south_east_even_x():
    assert Direction.SOUTH_EAST.apply((0, 0)) == (1, -1)


def test_apply_south_east_odd_x():
    assert Direction.SOUTH_EAST.apply((1, -1)) == (2, -1)
