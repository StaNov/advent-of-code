from adventofcode.problems.framework import AbstractSolver
from .internal import HexGrid


class Solver(AbstractSolver):
    def __init__(self):
        super().__init__()
        self.hex_grid = HexGrid()

    def _solve_1_internal(self, input_):
        self.hex_grid.move_directions(input_)
        return self.hex_grid.distance

    def _solve_2_internal(self, input_):
        self.hex_grid.move_directions(input_)
        return self.hex_grid.max_distance
