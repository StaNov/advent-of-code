import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


def test_example_1_vertical_collision(calculator):
    assert "3,3" == DayCalculator(
        r" /-\ " "\n"
        r" | v " "\n"
        r" | | " "\n"
        r" | | " "\n"
        r" | ^ " "\n"
        r" \-/ " "\n"
    ).calculate_part_1()


def test_example_1_horizontal_collision(calculator):
    assert "4,0" == DayCalculator(
        r" />---<\ " "\n"
        r" |     | " "\n"
        r" \-----/ " "\n"
    ).calculate_part_1()


def test_example_1_corner_collision_1(calculator):
    assert "1,1" == DayCalculator(
        r" /-<\ " "\n"
        r" |  | " "\n"
        r" |  | " "\n"
        r" ^  | " "\n"
        r" \--/ " "\n"
    ).calculate_part_1()


def test_example_1_corner_collision_2(calculator):
    assert "1,4" == DayCalculator(
        r" /--\ " "\n"
        r" v  | " "\n"
        r" |  | " "\n"
        r" |  | " "\n"
        r" \-</ " "\n"
    ).calculate_part_1()


def test_example_1_one_crossroad(calculator):
    assert "3,3" == DayCalculator(
        r"   /-----\   " "\n"
        r"   v     |   " "\n"
        r"   |     |   " "\n"
        r"   |     |   " "\n"
        r" />+-----+-\ " "\n"
        r" | \-----/ | " "\n"
        r" \---------/ " "\n"
    ).calculate_part_1()


def test_example_1_four_crossroad(calculator):
    assert "6,0" == DayCalculator(
        r" >-----+   " "\n"
        r"       +++ " "\n"
        r"         ^ " "\n"
    ).calculate_part_1()


def test_example_1_full(calculator):
    assert "7,3" == DayCalculator(
        r"/->-\         " "\n"
        r"|   |  /----\ " "\n"
        r"| /-+--+-\  | " "\n"
        r"| | |  | v  | " "\n"
        r"\-+-/  \-+--/ " "\n"
        r"  \------/    " "\n"
    ).calculate_part_1()


def test_main_1(calculator):
    assert "124,90" == calculator.calculate_part_1()


def test_example_2_one_collision(calculator):
    assert "4,0" == DayCalculator(
        r">----<-<" "\n"
    ).calculate_part_2()


def test_example_2_two_collisions(calculator):
    assert "5,0" == DayCalculator(
        r"><-><-<" "\n"
    ).calculate_part_2()


def test_example_2_collision_preservation(calculator):
    assert "1,0" == DayCalculator(
        r"><" "\n"
        r" ^" "\n"
    ).calculate_part_2()


def test_example_2_full(calculator):
    assert "6,4" == DayCalculator(
        r"/>-<\   " "\n"
        r"|   |   " "\n"
        r"| /<+-\ " "\n"
        r"| | | v " "\n"
        r"\>+</ | " "\n"
        r"  |   ^ " "\n"
        r"  \<->/ " "\n"
    ).calculate_part_2()


def test_main_2(calculator):
    assert "145,88" == calculator.calculate_part_2()
