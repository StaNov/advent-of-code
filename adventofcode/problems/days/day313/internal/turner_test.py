import pytest

from adventofcode.problems.days.day313.internal.direction import Direction
from adventofcode.problems.days.day313.internal.roadtype import RoadType
from adventofcode.problems.days.day313.internal.turner import Turner


class TestRoadType:
    @staticmethod
    def rotate(initial_direction):
        return Direction.UP


def test_new_direction_non_crossroad():
    assert Turner().new_direction(TestRoadType(), None) is Direction.UP


@pytest.mark.parametrize("turn_count,expected", [
    (0, Direction.LEFT),
    (1, Direction.UP),
    (2, Direction.RIGHT),
    (3, Direction.LEFT),
    (3*200 + 2, Direction.RIGHT),
])
def test_new_direction_crossroad(turn_count, expected):
    turner = Turner()
    for _ in range(turn_count):
        turner.new_direction(RoadType.CROSSROAD, Direction.UP)

    assert turner.new_direction(RoadType.CROSSROAD, Direction.UP) is expected
