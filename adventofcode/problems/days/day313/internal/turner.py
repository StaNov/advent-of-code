from adventofcode.problems.days.day313.internal.direction import Direction
from adventofcode.problems.days.day313.internal.roadtype import RoadType


_DIRECTIONS = [
    lambda direction: direction.value + 1,
    lambda direction: direction.value,
    lambda direction: direction.value - 1,
]


class Turner:
    def __init__(self):
        self._next_index = 0

    def new_direction(self, road_type, direction):
        if road_type is not RoadType.CROSSROAD:
            return road_type.rotate(direction)

        new_direction = Direction(_DIRECTIONS[self._next_index](direction) % len(Direction))
        self._next_index = (self._next_index + 1) % len(_DIRECTIONS)
        return new_direction
