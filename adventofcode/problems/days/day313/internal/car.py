from adventofcode.problems.days.day313.internal.turner import Turner


class Car:

    def __init__(self, position, direction):
        super().__init__()
        self.position = position
        self.direction = direction
        self.destroyed = False
        self.turner = Turner()

    def move(self):
        self.position = self.direction.apply_on(self.position)

    def turn(self, road_type):
        self.direction = self.turner.new_direction(road_type, self.direction)
