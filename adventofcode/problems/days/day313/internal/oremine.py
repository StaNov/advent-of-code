from .roadtype import RoadType


class OreMine:
    def __init__(self, cars, roads=None):
        if roads is None:
            roads = []

        self.cars = cars
        self.roads = {}

        for road_position, road_type in roads:
            self.roads[road_position] = road_type

    def simulate_step(self):
        self.cars.sort(key=lambda c: c.position)
        collisions = []

        for car in self.cars:
            try:
                self._move_and_turn_car(car)
            except CollisionHappened as e:
                collisions.append(e)

        self.cars = [car for car in self.cars if not car.destroyed]
        return collisions

    def _move_and_turn_car(self, car):
        car.move()
        self._check_collisions(car)
        road_under_car = self.roads.get(car.position, RoadType.STRAIGHT)
        car.turn(road_under_car)

    def _check_collisions(self, car):
        colliding_car = next(filter(
            lambda c:
                c is not car and
                car.position == c.position and
                not c.destroyed,
            self.cars), None)

        if not colliding_car:
            return

        car.destroyed = True
        colliding_car.destroyed = True
        raise CollisionHappened(car.position)

    def simulate_until_first_collision(self):
        while True:
            collisions = self.simulate_step()

            if collisions:
                return collisions[0].collision_position

    def simulate_until_last_car(self):
        while len(self.cars) > 1:
            self.simulate_step()

        return self.cars[0].position


class CollisionHappened(BaseException):
    def __init__(self, collision_position):
        super().__init__()
        self.collision_position = collision_position
