import pytest

from .solver import Solver


@pytest.fixture
def solver():
    return Solver()


class TestOreMine:
    @staticmethod
    def simulate_until_first_collision():
        return 1, 2

    @staticmethod
    def simulate_until_last_car():
        return 3, 4


def test_part_1(solver):
    assert "1,2" == solver.solve_1(TestOreMine())


def test_part_2(solver):
    assert "3,4" == solver.solve_2(TestOreMine())
