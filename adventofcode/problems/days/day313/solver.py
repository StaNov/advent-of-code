from adventofcode.problems.framework import AbstractSolver


class Solver(AbstractSolver):

    def _solve_1_internal(self, input_):
        collision_position = input_.simulate_until_first_collision()
        position_x, position_y = collision_position
        return "{},{}".format(position_x, position_y)

    def _solve_2_internal(self, input_):
        last_car_position = input_.simulate_until_last_car()
        position_x, position_y = last_car_position
        return "{},{}".format(position_x, position_y)
