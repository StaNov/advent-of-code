from .inputtextparser import InputTextParser


def test_parse():
    assert InputTextParser().parse(".#.\n#.#\n") == [[False, True, False], [True, False, True]]
