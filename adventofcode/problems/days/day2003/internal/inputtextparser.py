from adventofcode.problems.framework import DefaultInputTextParser


class InputTextParser(DefaultInputTextParser):
    def parse(self, input_string):
        lines = input_string.splitlines()

        return list(map(lambda line: list(map(lambda char: char == "#", line)), lines))
