from adventofcode.problems.framework import AbstractSolver


class Solver(AbstractSolver):
    def _solve_1_internal(self, input_):
        return calculate_slope((3, 1), input_)

    def _solve_2_internal(self, input_):
        slopes = ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2))
        result = 1

        for slope in slopes:
            result *= calculate_slope(slope, input_)

        return result


def calculate_slope(slope, terrain):
    slope_x, slope_y = slope
    width = len(terrain[0])
    x = 0
    y = 0
    result = 0

    while y < len(terrain):
        if terrain[y][x]:
            result += 1
        x += slope_x
        x %= width
        y += slope_y

    return result
