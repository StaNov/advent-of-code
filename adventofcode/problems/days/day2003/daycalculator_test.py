import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


def test_example_1_1(calculator):
    assert DayCalculator("""
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
""").calculate_part_1() == 7


def test_main_1(calculator):
    assert calculator.calculate_part_1() == 176


def test_example_2_1(calculator):
    assert DayCalculator("""
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
""").calculate_part_2() == 336


def test_main_2(calculator):
    assert calculator.calculate_part_2() == 5872458240
