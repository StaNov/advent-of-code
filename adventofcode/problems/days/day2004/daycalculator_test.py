import pytest

from .daycalculator import DayCalculator


@pytest.fixture
def calculator():
    return DayCalculator()


def test_example_1_1(calculator):
    assert DayCalculator("""
ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in""").calculate_part_1() == 2


def test_main_1(calculator):
    assert calculator.calculate_part_1() == 237


def test_example_2_1(calculator):
    # TODO
    # assert DayCalculator("Part 2, example 1").calculate_part_2() == 0
    pass


def test_example_2_2(calculator):
    # TODO
    # assert DayCalculator("Part 2, example 2").calculate_part_2() == 0
    pass


def test_main_2(calculator):
    # TODO
    # assert calculator.calculate_part_2() == 0
    pass
