from .passport import Passport


def test_valid_password():
    passport = Passport()

    passport.eye_color = "test"
    passport.passport_id = "test"
    passport.expiration_year = "test"
    passport.hair_color = "test"
    passport.birth_year = "test"
    passport.issued_year = "test"
    passport.height = "test"

    assert passport.is_valid()


def test_invalid_password():
    assert not Passport().is_valid()
