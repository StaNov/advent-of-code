from .inputtextparser import InputTextParser


def test_parse_passport_id():
    parsed = InputTextParser().parse("pid:1234")

    assert len(parsed) == 1
    assert parsed[0].passport_id == "1234"


def test_parse_birth_year():
    parsed = InputTextParser().parse("byr:2001")

    assert len(parsed) == 1
    assert parsed[0].birth_year == "2001"


def test_parse_issue_year():
    parsed = InputTextParser().parse("iyr:2015")

    assert len(parsed) == 1
    assert parsed[0].issued_year == "2015"


def test_parse_expiration_year():
    parsed = InputTextParser().parse("eyr:2023")

    assert len(parsed) == 1
    assert parsed[0].expiration_year == "2023"


def test_parse_height():
    parsed = InputTextParser().parse("hgt:123cm")

    assert len(parsed) == 1
    assert parsed[0].height == "123cm"


def test_parse_hair_color():
    parsed = InputTextParser().parse("hcl:#ffffff")

    assert len(parsed) == 1
    assert parsed[0].hair_color == "ffffff"


def test_parse_eye_color():
    parsed = InputTextParser().parse("ecl:brn")

    assert len(parsed) == 1
    assert parsed[0].eye_color == "brn"


def test_parse_country_id():
    parsed = InputTextParser().parse("cid:987654")

    assert len(parsed) == 1
    assert parsed[0].country_id == "987654"


def test_multiple_attributes():
    parsed = InputTextParser().parse("cid:1234 pid:5678")

    assert len(parsed) == 1
    assert parsed[0].country_id == "1234"
    assert parsed[0].passport_id == "5678"


def test_multiple_lines():
    parsed = InputTextParser().parse("cid:1234\npid:5678")

    assert len(parsed) == 1
    assert parsed[0].country_id == "1234"
    assert parsed[0].passport_id == "5678"


def test_multiple_passports():
    parsed = InputTextParser().parse("cid:1234\npid:5678\n\ncid:9876\npid:5432")

    assert len(parsed) == 2
    assert parsed[0].country_id == "1234"
    assert parsed[0].passport_id == "5678"
    assert parsed[1].country_id == "9876"
    assert parsed[1].passport_id == "5432"
