class Passport:
    def __init__(self):
        self.eye_color = None
        self.passport_id = None
        self.expiration_year = None
        self.hair_color = None
        self.birth_year = None
        self.issued_year = None
        self.country_id = None
        self.height = None

    def is_valid(self):
        return (self.eye_color is not None
                and self.passport_id is not None
                and self.expiration_year is not None
                and self.hair_color is not None
                and self.birth_year is not None
                and self.issued_year is not None
                and self.height is not None
                )
