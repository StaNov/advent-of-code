import re

from .passport import Passport
from adventofcode.problems.framework import DefaultInputTextParser


class InputTextParser(DefaultInputTextParser):
    def parse(self, input_string):
        result = []

        for passport_string in input_string.split("\n\n"):
            result.append(_parse_passport(passport_string))

        return result


def _parse_passport(input_string):
    result = Passport()

    for attribute in input_string.split():
        key, value = re.match(r"^(.+):(.+)$", attribute).groups()

        if key == "pid":
            result.passport_id = value
        elif key == "byr":
            result.birth_year = value
        elif key == "iyr":
            result.issued_year = value
        elif key == "eyr":
            result.expiration_year = value
        elif key == "hgt":
            result.height = value
        elif key == "hcl":
            result.hair_color = value[1:]
        elif key == "ecl":
            result.eye_color = value
        elif key == "cid":
            result.country_id = value

    return result
